/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04ox;

/**
 *
 * @author informatics
 */
public class Table {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private final Player player1;
    private final Player player2;
    public Player currentPlayer;
    private int turnCount;
    public Table(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public void switchPlayer(){
        turnCount++;
        if(currentPlayer == player1){
            currentPlayer = player2;
        }else{
            currentPlayer= player1;
        }
    }
    public boolean checkWin(){
        if(checkRow()){
            return true;     
        }else if(cheakCol()){
            return true; 
        }else if(cheakCross1()){
            return true; 
        }else if(cheakCross2()){
            return true; 
        }
        return false;
    }
    private boolean checkRow(){
        if((table[0][0] == currentPlayer.getSymbol() && table[0][1] == currentPlayer.getSymbol() && table[0][2] == currentPlayer.getSymbol())||
           (table[1][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[1][2] == currentPlayer.getSymbol())||
           (table[2][0] == currentPlayer.getSymbol() && table[2][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol())){
            return true;
        }
        return false;
    }
    
    private boolean cheakCol() {
        if((table[0][0] == currentPlayer.getSymbol() && table[1][0] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol())||
           (table[0][1] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][1] == currentPlayer.getSymbol())||
           (table[0][2] == currentPlayer.getSymbol() && table[1][2] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol())){
            return true;
        }
        return false;

    }

    private boolean cheakCross1() {
        if(table[0][0] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][2] == currentPlayer.getSymbol()){
            return true;
        }
        return false;
    }


    private boolean cheakCross2() {
        if(table[0][2] == currentPlayer.getSymbol() && table[1][1] == currentPlayer.getSymbol() && table[2][0] == currentPlayer.getSymbol()){
            return true;
        }
        return false;
    }

    boolean checkDraw(){
        if(turnCount==9){
            return true;
        }
        return false;
    }

    public boolean setPos(int inputPos) {
        if(inputPos=='-') {
            inputPos = currentPlayer.getSymbol();
            return true;
        }
        return false;
    }
}
