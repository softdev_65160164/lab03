/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04ox;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;
    static String pos;  

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }
    
    public void newGame(){
        this.table = new Table(player1, player2);
    }
    public void play() {
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputPos();
            if(table.checkWin()){
                newGame();
                printWin();
                break;
            }
            if(table.checkDraw()){
                newGame();
                printDraw();
                break;
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0;i<3;i++){
            for(int j = 0; j<3; j++){
                System.out.print(t[i][j]+ " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputPos() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input position(1-9) : ");
            int inputPos = kb.nextInt();
            char[][]t = table.getTable();
            switch(inputPos){
                case 1:
                    t[0][0] = table.getCurrentPlayer().getSymbol();
                    break;
                case 2:
                    t[0][1] = table.getCurrentPlayer().getSymbol();
                    break;
                case 3:
                    t[0][2] = table.getCurrentPlayer().getSymbol();
                    break;
                case 4:
                    t[1][0] = table.getCurrentPlayer().getSymbol();
                    break;
                case 5:
                    t[1][1] = table.getCurrentPlayer().getSymbol();
                    break;
                case 6:
                    t[1][2] = table.getCurrentPlayer().getSymbol();
                    break;
                case 7:
                    t[2][0] = table.getCurrentPlayer().getSymbol();
                    break;
                case 8:
                    t[2][1] = table.getCurrentPlayer().getSymbol();
                    break;
                case 9:
                    t[2][2] = table.getCurrentPlayer().getSymbol();
                    break;
                default:
                    System.out.println("!!!Please Input only(1-9)!!!");
            }
            table.setPos(inputPos);
            break;

        }
        
    }
    private void printWin() {
        System.out.println("Congratulation! Player "+table.getCurrentPlayer().getSymbol()+" win!");
    }
    
    private void printDraw() {
        System.out.println("The game ended in the draw!");
    }

}
